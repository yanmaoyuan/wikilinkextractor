#include <iostream>
#include <string>
#include <curl/curl.h> 
#include "gumbo.h"
#include <stdlib.h>
#include <vector>
using namespace std;
//stores the page html 
string page_content;

//it stores the content of a url to page_content
size_t writeCallback(char* buf, size_t size, size_t nmemb, void* up) 
{
    for (int c = 0; c<size*nmemb; c++)
    {   
        page_content.push_back(buf[c]);
    }   
    return size*nmemb; //tell curl how many bytes we handled
}


//determine the locations where wanted links might reside
static void extract_links(GumboNode* node, vector<string> &hrefs) {
  if(node->type == GUMBO_NODE_ELEMENT) {
     // we only consider links in tag <p>
     if(node->v.element.tag == GUMBO_TAG_P){
       GumboVector* children = &node->v.element.children;
       // iterate through all of the children of a <p> tag content
       for (int i = 0; i < children->length; ++i) {
         GumboNode* node = static_cast<GumboNode*>(children->data[i]);
         if(node->type == GUMBO_NODE_ELEMENT){
            GumboAttribute* href;
            //extract all of the hrefs from selected areas
            if(node->v.element.tag == GUMBO_TAG_A &&
                (href = gumbo_get_attribute(&node->v.element.attributes, "href"))){
                 //store the extracted href to a vector
                 hrefs.push_back(href->value);
             }
          }
       }   
   }

   //resursively search for the location
   GumboVector* children = &node->v.element.children;
     for (int i = 0; i < children->length; ++i){
        extract_links(static_cast<GumboNode*>(children->data[i]), hrefs);
     }
   }
}

//given a url, extract the links from the content area
static vector<string> get_links(char* url){
  //stores links extracted from the wiki page
  vector<string> hrefs;
  
  CURL* curl; //curl object
  //setting up curl to get the content of the page
  curl_global_init(CURL_GLOBAL_ALL); 
  curl = curl_easy_init();
  
  curl_easy_setopt(curl, CURLOPT_URL, url);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);

  curl_easy_perform(curl);

  curl_easy_cleanup(curl);
  curl_global_cleanup();
   
  //now we have the html extracted, we can now fetch the links that we want
  GumboOutput* output = gumbo_parse(page_content.c_str());

  //call find the specific links within the target locations
  extract_links(output->root, hrefs);

  gumbo_destroy_output(&kGumboDefaultOptions, output);    
  return hrefs;
}


int main(int argc, char** argv)
{
    if( argc != 2){
      fprintf(stderr, "Usage: require input <url>");
      exit(EXIT_FAILURE);
    }

  vector<string> hrefs = get_links(argv[1]);
  for(vector<string>::iterator it = hrefs.begin(); it < hrefs.end(); it++){
    cout << *it << endl;
  }

  return 0;
}

