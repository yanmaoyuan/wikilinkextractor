#include <iostream>
#include <string>
#include <curl/curl.h> 
#include "gumbo.h"
#include <stdlib.h>
using namespace std;
//make a function call; converting it to C if possible
//stores the page html 
string page_content;

//find all of the hrefs of a given area
static void find_links(GumboNode* node){
   if(node->type != GUMBO_NODE_ELEMENT){
     return;
   }   
   GumboAttribute* href;
   if(node->v.element.tag == GUMBO_TAG_A &&
      (href = gumbo_get_attribute(&node->v.element.attributes, "href"))){
      cout << href->value << endl;
   }   
}

//determine the locations where wanted links might reside
static void find_link_locs(GumboNode* node) {
  if (node->type != GUMBO_NODE_ELEMENT) {
     return;
  }   

  if(node->v.element.tag == GUMBO_TAG_P){
    GumboVector* children = &node->v.element.children;
 
    for (int i = 0; i < children->length; ++i) {
       find_links(static_cast<GumboNode*>(children->data[i]));
    }   
  }
 //resursively search for the location
  GumboVector* children = &node->v.element.children;
    for (int i = 0; i < children->length; ++i){
       find_link_locs(static_cast<GumboNode*>(children->data[i]));
    }   
}

//this part is borrowed from one of the examples in curl
//it stores the content of a url to page_content
size_t writeCallback(char* buf, size_t size, size_t nmemb, void* up) 
{
    for (int c = 0; c<size*nmemb; c++)
    {   
        page_content.push_back(buf[c]);
    }   
    return size*nmemb; //tell curl how many bytes we handled
}

int main(int argc, char** argv)
{
    if( argc != 2){
      fprintf(stderr, "Usage: require input <url>");
      exit(EXIT_FAILURE);
    }

    CURL* curl; //curl object

    curl_global_init(CURL_GLOBAL_ALL); 
    curl = curl_easy_init();
    
    curl_easy_setopt(curl, CURLOPT_URL, argv[1]);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &writeCallback);
    //curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L); //tell curl to output its progress

    curl_easy_perform(curl);


    curl_easy_cleanup(curl);
    curl_global_cleanup();
   
    //now we have the html extracted, we can now fetch the links that we want
    GumboOutput* output = gumbo_parse(page_content.c_str());
    find_link_locs(output->root);
    gumbo_destroy_output(&kGumboDefaultOptions, output);    

    return 0;
}

